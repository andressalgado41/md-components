package com.andressalgadoc.materialdesigned.utils;

import com.andressalgadoc.materialdesigned.framents.BottomNavigationBarFragment;
import com.andressalgadoc.materialdesigned.framents.ButtonFragment;
import com.andressalgadoc.materialdesigned.framents.CheckBoxFragment;
import com.andressalgadoc.materialdesigned.framents.FloatingActionButtonFragment;
import com.andressalgadoc.materialdesigned.framents.SnackBarFragment;
import com.andressalgadoc.materialdesigned.framents.TextFieldFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class CommonUtils {

    public static void setFragment (AppCompatActivity activity, String nameFragment, int contentRes) {

        Fragment fragment = getFragmentById(nameFragment);
        activity.getSupportFragmentManager().beginTransaction().add(contentRes, fragment).commit();

    }

    private static Fragment getFragmentById(String nameFragment) {

        Fragment fragment = null;

        switch (nameFragment) {
            //SCROLL
            case ButtonFragment.TAG:
                fragment = new ButtonFragment();
                break;
            case TextFieldFragment.TAG:
                fragment = new TextFieldFragment();
            case CheckBoxFragment.TAG:
                fragment = new CheckBoxFragment();
                break;
            // STATTIC
            case BottomNavigationBarFragment.TAG:
                fragment = new BottomNavigationBarFragment();
                break;
            case SnackBarFragment.TAG:
                fragment = new SnackBarFragment();
                break;
            case FloatingActionButtonFragment.TAG:
                fragment = new FloatingActionButtonFragment();
                break;
        }

        return fragment;

    }

}
