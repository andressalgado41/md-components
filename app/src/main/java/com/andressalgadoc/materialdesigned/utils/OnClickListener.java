package com.andressalgadoc.materialdesigned.utils;

public interface OnClickListener {
    void onClick(Component component);
}
